<?php
ob_start();
// Define path to application directory (התיקייה שבה קיימת האפליקציה)
defined('APPLICATION_PATH')
    || define('APPLICATION_PATH', realpath(dirname(__FILE__)));

//this function loads Class files automatically 
function __autoload($path) {
	return include str_replace('_', '/', $path) . '.php'; // בונה משם המחלקה את השם של הקובץ, מחליפה את האנדר סקור בסלאש ומוסיפה נקודה פי אייץ פי
}
$rest = new Rest(); //יוצר אובייקט מסוג רסט
$rest->process(); //(מפעיל עליו את הפונקציה פרוסס שנמצאת ברסט (מפעילים אותה על רסט לכן ניתן לדעת שהיא ברסט
ob_end_flush();
