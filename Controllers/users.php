<?php
class Controllers_Users extends RestController {
	private $_con;
	private function DBconnect(){
		$this->_con=mysqli_connect("localhost","hilasc","19642","hilasc_rest_api");
	}
	public function get() { 
		$this->DBconnect();
		//var_dump($this->request['params']['id']);
		//die();
		if (isset($this->request['params']['id'])){ // if not null (ID as params, users/1)
			if(!empty($this->request['params']['id'])) { //ID exists in URL
				$sql="SELECT * FROM users WHERE id=?";				// gets the details of the user
				$stmt = $this->_con->prepare($sql);
				$stmt->bind_param("s",$this->request['params']['id']); //prepare statement
				$stmt->execute();
				$stmt->store_result();				
				$stmt->bind_result($id, $name,$email);
				$stmt->fetch();
				$result = ['id'=>$id,'name'=>$name,'email'=>$email];
				$this->response = array('result' =>$result );
				$this->responseStatus = 200;
			} else { //ID does not exists in URL
				$sql = "SELECT * FROM users"; //gets the details of all users
				$usersSqlResult = mysqli_query($this->_con, $sql);
				$users = []; //users array
				while ($user = mysqli_fetch_array($usersSqlResult)){
					$users[] = ['name' => $user['name'], 'email' => $user['email']]; //add new details to the array
				}
				$this->response = array('result' => $users); // array with key results and value users //the rest.php already converted this to json (see rest.php encode json function, jsonResponse)
				$this->responseStatus = 200;
			}	
		}else{
			$this->response = array('result' =>'Wrong parameters for users' );
			$this->responseStatus = 200;			
		}
	}
	public function post() { //on post we get json object to insert
		$this->DBconnect();
		if(isset($this->request['params']['payload'])){ //request params is everything after the ? in URL and payload is all after the ? in URL
														//null check (if payload is null)
			if (!empty($this->request['params']['payload'])) { //if params (or payload) is not null but empty (example: ?param="")
				$user_array = json_decode($this->request['params']['payload'],true); //json to array
				$name = $user_array['name']; //get the name parameter from array
				$email = $user_array['email']; //get the email parameter from array
				$sql = "INSERT INTO users (name, email) VALUES (?,?)"; //query (? is where the parameter goes (name,email))
				$stmt = $this->_con->prepare($sql);
				$stmt->bind_param("ss", $name, $email); //prepare statement
				$stmt->execute(); //run the query
				$result = $stmt->insert_id; //gives the last inserted user id
				$this->responseStatus = 200;
				$this->response = array('result' =>'User with id '.$result.' was added'); //the message we recieved
				return;
			}
		}

		else { //if json in payload was wrong
			$this->response = array('result' =>'Wrong parameters for create new users' );
			$this->responseStatus = 200;
		}
	}
	
	public function put() {
		$this->response = array('result' => 'no put implemented for users');
		$this->responseStatus = 200;
	}
	public function delete() {
		$this->response = array('result' => 'no delete implemented for users');
		$this->responseStatus = 200;
	}
}
